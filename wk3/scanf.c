#include <stdio.h>

int main(void) {
	int x;
	printf("Please enter a number.\n");
	if (scanf("%d", &x) != 1) {
		fprintf(stderr, "no number given\n");
		return 1;
	}
	printf("Your number was: %d\n", x);
	return 0;
}
