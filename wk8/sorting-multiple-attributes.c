#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
	char unikey[9];
	int mark;
} MarkRow;

char buffer[256];
static MarkRow marks[20];

int MarkRowCompare(const void *x, const void *y) {
	const MarkRow *a = x;
	const MarkRow *b = y;

	if(a->mark == b->mark) {
		return strcmp(a->unikey, b->unikey);
	} else {
		return (b->mark - a->mark);
	}
}

int MarkCompare(const void *x, const void *y) {
	const int *a = x;
	const MarkRow *b = (MarkRow *) y;

	return (b->mark - *a);
}

int main(int argc, char const *argv[])
{
	for(int i = 0; i < 20; i++){
		int num;
		char unikey2[9];
		

		fgets(buffer, 256, stdin);
		sscanf(buffer, "%8s %d", unikey2, &num);

		MarkRow newRow;
		strcpy(newRow.unikey, unikey2);
		newRow.mark = num;
		marks[i] = newRow;
	}


	qsort(&marks[0], 20, sizeof(MarkRow), MarkRowCompare);

	for (int j = 0; j < 20; j++) {
		printf("%s %d\n", marks[j].unikey, marks[j].mark);
	}

	int key = 74;
	MarkRow *search; 
	search = (MarkRow *) bsearch(&key, &marks[0], 20, sizeof(MarkRow), MarkCompare);

	if(search == NULL) {
		printf("key not found.\n");
	} else {
		printf("%s %d\n", search->unikey, search->mark);
	}
	return 0;
}