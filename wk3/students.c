#include <stdio.h>

char *names[] = {"Bob", "Jane Doe", "John Smith"};
char *sids[] = {"12345678", "23456789", "98765432"};
double marks[] = {99.8763, 78.29561, 62.11345};

int main(void) {
	int i;
	printf("Name	   SID	    Mark\n");
	for (i = 0; i < 3; i++) {
		printf("%-10s %s %.2f\n", names[i], sids[i], marks[i]);
	}

	return 0;
}
