#include <stdio.h>
#include "myheader.h"

int main(int argc, char **argv) {
    int i, j;
    for (i = 1; i <= LOOPBOUND; i++) {
        for (j = 1; j <= LOOPBOUND; j++) {
            printf("%d ", mymin(i, j));
        }
        printf("\n");
    }
    return 0;
}
