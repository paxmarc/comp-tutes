#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
    int buflen = 8;
    char *buf = (char *) malloc(buflen * sizeof(char));

    int len;
    while (fgets(buf, buflen, stdin) != NULL) {
        len = strlen(buf);
        printf("(%d) %s\n", len, buf);
    }

    free(buf);
    return 0;

}
