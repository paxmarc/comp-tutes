#include <stdio.h>
#include <stdlib.h>
#include "queue.h"

Queue *queue_create(void) { 

    Queue *to_return = (Queue *) malloc(sizeof(Queue));
    Queue q = {NULL, NULL};

    *to_return = q;

    return to_return;
}

int queue_isempty(Queue *q) {
    if(q->front == NULL || q->back == NULL) {
        return 1;
    } else
        return 0;
}

void queue_push(Queue *q, int input) {

    Node *newNode = (Node *) malloc(sizeof(Node));
    Node n = {input, NULL, NULL};

    *newNode = n;

    /* Case for empty queue */
    if(queue_isempty(q)) {
        q->front = newNode;
        q->back = newNode;

        return;
    }

    /* Normal case */
    newNode->prev = q->back;
    q->back = newNode;

    return;
}

int queue_pop(Queue *q) {
    Node *toPop = q->front;

    int toReturn = toPop->data;

    q->front = toPop->next;

    free(toPop);

    return toReturn;
}

void queue_delete(Queue *q) {

    if(!queue_isempty(q)) {
        Node *current = q->front;
        Node *next;


        while(current->next != NULL) {
            next = current->next;
            free(current);
            current = next;
        }

        free(current);
    }

    free(q);

    return;

}
