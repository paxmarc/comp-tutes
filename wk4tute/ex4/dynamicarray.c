#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[]) {

    if (argc != 2)
        fprintf(stderr, "Please provide an integer value.");

    int arraylen = atoi(argv[1]);
    int *array = (int *) malloc(arraylen * sizeof(int));

    int i = 0;

    for(i = 0; i < arraylen; ++i) {
        if (i == 0) 
            array[i] = (i + 1) * (i + 1);
        else
            array[i] = array[i-1] + ((i + 1) * (i + 1));
    }

    printf("%d\n", array[arraylen-1]);

    free(array);

    return 0;
}

