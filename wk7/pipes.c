#include <stdio.h>
#include <unistd.h>
#include <string.h>

#define MAXBUFFER 128;

int main(void) {

	pid_t pid;
	int pipefd[2];
	if (pipe(pipefd) < 0) {
		perror("Could not create pipe");
		// Handle error
	}

	pid = fork();
	if (pid < 0) {
		perror("Fork failed");
		return 1;
	}

	if (pid == 0) {  //Child
		close(pipefd[1]);
		char str[14];
		read(pipefd[0], str, 14);
		printf("%s\n", str);
		close(pipefd[0]);

		return 0;

	} else {
		close(pipefd[0]);
		char *str = "Hello there!";
		printf("Writing \"%s\" to pipe.\n", str);
		write(pipefd[1], str, strlen(str));
		close(pipefd[1]);
	}

	return 0;
}