#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define NTHREADS 4

typedef struct {
	int *array;
	int start, end;
	int answer;
} WorkerArgs;

void *worker(void *arg) {
	
	printf("%i\n", __LINE__);

	WorkerArgs *wargs = (WorkerArgs *) arg;
	printf("Start:%i End:%i \n", wargs->start, wargs->end);
	int answer = wargs->answer;
	for(int i = wargs->start; i <= wargs->end; i++) {
		if(i == wargs->start) {
			wargs->array[i] = i;
		} else {
			wargs->array[i] = wargs->array[i-1] + i;
		}
	}

	wargs->answer = wargs->array[wargs->end];

	printf("Answer:%i\n", wargs->answer);

	return NULL;
}

int main(int argc, char const *argv[])
{
	int nums[1001];

	pthread_t threads[NTHREADS];

	WorkerArgs *wargs[4];

	for(int i = 0; i < NTHREADS; i++) {
		wargs[i] = (WorkerArgs *) malloc(sizeof(WorkerArgs));
	}

	wargs[0]->array = nums;
	wargs[0]->start = 0;
	wargs[0]->end = 250;
	wargs[0]->answer = 0;

	wargs[1]->array = nums;
	wargs[1]->start = 251;
	wargs[1]->end = 500;
	wargs[1]->answer = 0;

	wargs[2]->array = nums;
	wargs[2]->start = 501;
	wargs[2]->end = 750;
	wargs[2]->answer = 0;

	wargs[3]->array = nums;
	wargs[3]->start = 751;
	wargs[3]->end = 1000;
	wargs[3]->answer = 0;


	for (int i = 0; i < NTHREADS; i++)
 		pthread_create(&threads[i], NULL, worker, (void *) wargs[i]);

 	for (int i = 0; i < NTHREADS; i++){
 		pthread_join(threads[i], NULL);
 	}

 	long sum = 0;
	for (int i = 0; i < NTHREADS; i++) {
	 sum += wargs[i]->answer;
	}

	free(wargs[0]);
	free(wargs[1]);
	free(wargs[2]);
	free(wargs[3]);

	printf("Sum:%ld\n", sum);

	return 0;
}