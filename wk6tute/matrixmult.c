#include <stdio.h>

void matrix_vector_mult(int matrix[3][3], int ivector[3], int ovector[3]);

void print_matrix(int matrix[3][3]);
void print_vector(int vector[3]);

int main(void) {

	  int matrix[3][3] = {
    {1, 0, 1},
    {0, -1, -2},
    {-1, 1, 0}
  };

  int vector[3] = {1, 1, 1};
  int product[3];

  printf("Matrix:\n");
  print_matrix(matrix);

  printf("\nVector:\n");
  print_vector(vector);

  printf("\nProduct:\n");
  matrix_vector_mult(matrix, vector, product);
  print_vector(product);

  return 0;

}

void matrix_vector_mult(int matrix[3][3], int ivector[3], int ovector[3]) {

	for(int i = 0; i < 3; i++) {
		int product = 0;
		for(int j = 0; j < 3; j++) {
			product = product + (matrix[i][j]*ivector[j]);
		}
		ovector[i] = product;
	}
}

void print_matrix(int matrix[3][3]) {
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      printf("%2d ", matrix[i][j]);
    }
    printf("\n");
  }
}

void print_vector(int vector[3]) {
  for (int i = 0; i < 3; i++) {
    printf("%d\n", vector[i]);
  }
}