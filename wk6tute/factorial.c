#include <stdio.h>

long factorial(const int n) {
	long result = 1;
	if (n > 0)
		result = n * factorial(n-1);
	return result;
}

int main(void) {
	printf("%ld\n", factorial(10));
	return 0;
}