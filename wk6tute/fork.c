#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main(void) {
	int n = 6;
	pid_t pid;

	printf("Before fork\n");
	pid = fork();
	if(pid < 0) {
		perror("Fork failed");
		return 1;
	}

	if(pid == 0) { //Child
		n++;
		sleep(1);
	} else {
		n *= 2;
		wait(NULL);
	}

	printf("Fork returned %d, n is %d\n", pid, n);
	return 0;
}