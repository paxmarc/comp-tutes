#include <stdio.h>
#include <stdlib.h>

static int nums[10] = {4, -2, 69, 7, 98, 0, -40, 6, 20, 55};

int intcompare(const void *x, const void *y) {
	const int *a = x;
	const int *b = y;

	return (*a - *b);
}

int main(int argc, char const *argv[]) {
	qsort(&nums[0], 10, sizeof(int), intcompare);

	for(int i = 0; i < 10; i++)
		printf("%d\n", nums[i]);

	return 0;
}
