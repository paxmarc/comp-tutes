#include <stdio.h>
#include <inttypes.h>

union int8bits {
int8_t val;
	struct {
		uint8_t rest : 7;
		uint8_t msb : 1;
		} parts;
 };

 void turn_on_msb(union int8bits);

 int main() {
 	union int8bits ibits;
 	for (ibits.val = -3; ibits.val <= 3; ibits.val++) {
 		printf("Value: %4d, First: %d, Rest: %3d\n",
 			ibits.val, ibits.parts.msb, ibits.parts.rest);
		}

	union int8bits ibits2;
	ibits2.val = 5;
	turn_on_msb(ibits2);

	union int8bits ibits3;
	ibits3.val = -5;
	turn_on_msb(ibits3);

 return 0;
}

void turn_on_msb(union int8bits u) {
	u.parts.msb = 1;
	printf("Value: %4d, First: %d, Rest: %3d\n",
 			u.val, u.parts.msb, u.parts.rest);
}
