#include <stdio.h>
#include <pthread.h>
#include <time.h>

#define NTHREADS (4)
#define BIGNUM   (100000)

clock_t tick, tock;

long long unsigned int bignu = BIGNUM;
 
long long unsigned int counter = 0;
pthread_mutex_t lock;

void *worker(void *arg) {
  
pthread_mutex_lock(&lock);
  for (long long unsigned int i = 0; i < BIGNUM; i++) {
    
    counter++;
    
  }
pthread_mutex_unlock(&lock);

  return NULL;
}

int main(void) {
  pthread_t threads[NTHREADS];
  pthread_mutex_init(&lock, NULL);

  tick = clock();

  for (int i = 0; i < NTHREADS; i++)
    pthread_create(&threads[i], NULL, worker, NULL);
  
  tock = clock();

  printf("tick:%f\n", (double) tick);
  printf("tock:%f\n", (double) tock);

  printf("tps:%d\n", CLOCKS_PER_SEC);
  printf("Time elapsed: %f seconds\n", (double) (tock - tick) / CLOCKS_PER_SEC);

  for (int i = 0; i < NTHREADS; i++)
    pthread_join(threads[i], NULL);

  pthread_mutex_destroy(&lock);

  printf("counter  = %llu\n", counter);
  printf("expected = %llu\n", NTHREADS * bignu);
  return 0;
}

