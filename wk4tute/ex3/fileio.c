#include <stdio.h>

char buf[1024];

int main(int argc, char *argv[]) {
    int count = 0;
    FILE *fp = fopen(argv[1], "r");
    while (fgets(buf, sizeof(buf), fp) != NULL) {
        printf("%s", buf);
    }
    fclose(fp);
    return 0;
}
